import React, { useState } from 'react';
import ApiService from './service/ApiService';
import Button from './components/button';
import ModalForm from './components/ModalForm';
import './App.css';
import logo from './logo.svg';

function App() {
  const [formData, setFormData] = useState(null);

  async function askQuestion(e){
    e.preventDefault();

   const fetchedFormData = await ApiService.getcustomerMessageFormData();
   await setFormData(fetchedFormData);
  }

  async function sendAnswearError(e){
    e.preventDefault();

   const fetchedFormData = await ApiService.getcustomerMessageErrorData();
   
   await setFormData(fetchedFormData);
  }

  async function sendAnswearSuccess(e){
    e.preventDefault();

   const fetchedFormData = await ApiService.getcustomerMessageFormSuccessData();
   await setFormData(fetchedFormData);
  }

  const message = formData?.message;

  return (   
   
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        { (formData && !message) && <ModalForm form={formData.form} sendAnswearError={sendAnswearError} sendAnswearSuccess={sendAnswearSuccess} /> }
        { !formData && <Button name='Задать вопрос' clickHandler = {askQuestion}/>}
        {message && <h2>{message}</h2>}
      </header>
    </div>
  );
}

export default App;
