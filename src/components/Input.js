import React  from 'react'


export const Input = ({inputData}) =>{

  return ( 
    <input type={inputData.type} name={inputData.name} {...inputData.attrs} defaultValue={inputData.value || ''} />
  )
}