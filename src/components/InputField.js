
import { Input } from './Input';
import { Select } from './Select';
import { TextArea } from './TextArea';

 const InputField = ({inputData}) =>{
    const InputComponents = {
        input: Input,
        select: Select,
        textarea: TextArea
    };

    const getProperInput = (type) =>
    InputComponents.hasOwnProperty(type)
    ? InputComponents[type]
    : InputComponents.input;
    const isSelect = (inputType) => inputType ==='select';

   
    const ProperInput = getProperInput(inputData.type);
 
    return (

        <label className={isSelect(inputData.type) ? 'selectOption' : ''}>
          
            {inputData.errors && <span>{inputData.errors}</span>}
            <ProperInput inputData= {inputData}/>
        </label>
    );
}

export default InputField;
