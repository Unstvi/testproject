import React from 'react'
import Button from './button'
import InputField from './InputField'

const ModalForm = ({form, sendAnswearError, sendAnswearSuccess}) => {
    return (
       <form onSubmit={(e) => e.preventDefault()}>
            {form.map((inputData, index) => <InputField inputData={inputData} key={index}/>)}
            { !form.message && <Button name='отправить с ошибкой' clickHandler={sendAnswearError}/> }
            { !form.message && <Button name='отправить с успехом' clickHandler={sendAnswearSuccess}/> }
       </form>
    )
}

export default ModalForm
