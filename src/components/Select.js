import React, {useState} from 'react'


export const Select = ({inputData}) =>{
    const [isShowedPlaceholder, setIsShowedPlaceholder] = useState(false);

    return(

        <>
        
        <h5 className='placeholder'>
        {isShowedPlaceholder && inputData.attrs.placeholder}
        </h5>
        <select 
        defaultValue={inputData.value || ''}
         
        name={inputData.name}
        {...inputData.attrs}
        onChange={() => setIsShowedPlaceholder(true)}
        >
        <option value="" disabled hidden>
        {inputData.attrs.placeholder}
        </option>
            {inputData.options.map((option, index) => {
            return(
                <option value={Object.keys(option)} key={index}>

                    {Object.values(option)}

                </option>
            );
        })}
        </select>
        </>
    )
}