import React from 'react'

export const TextArea = ({inputData}) =>{
    return(
        <textarea type={inputData.type} name={inputData.name} {...inputData.attrs} defaultValue={inputData.value || ''}/>
    )
}
