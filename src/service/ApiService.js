
class ApiService{
    async getcustomerMessageFormData() {
        const customerMessageFormData = await fetch('http://localhost:3000/customer-message-form.json')
        .then((data) => data.json())
        .then(data => data);
        
       
        return customerMessageFormData;
    }

    async getcustomerMessageFormSuccessData() {
        const customerMessageFormSuccessData = await fetch('http://localhost:3000/customer-message-form-success.json')
        .then((data) => data.json())
        .then(data => data);
        
       
        return customerMessageFormSuccessData;
    }

    async getcustomerMessageErrorData() {
        const customerMessageFormErrorData = await fetch('http://localhost:3000/customer-message-form-error.json')
        .then((data) => data.json())
        .then(data => data);
        
       
        return customerMessageFormErrorData ;
    }
}

export default new ApiService;